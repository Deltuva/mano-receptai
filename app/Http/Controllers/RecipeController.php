<?php

namespace App\Http\Controllers;

class RecipeController extends Controller
{
    /**
     * Show all recipes
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('recipe.index');
    }
}

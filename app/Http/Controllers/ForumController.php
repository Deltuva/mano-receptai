<?php

namespace App\Http\Controllers;

class ForumController extends Controller
{
    /**
     * Forum
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('forum.index');
    }
}

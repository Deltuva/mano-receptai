<?php

namespace App\Http\Controllers;

class ArticleController extends Controller
{
    /**
     * Show all articles
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('article.index');
    }
}

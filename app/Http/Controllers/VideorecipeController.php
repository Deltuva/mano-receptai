<?php

namespace App\Http\Controllers;

class VideorecipeController extends Controller
{
    /**
     * Show all videorecipes
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('videorecipe.index');
    }
}

<?php

namespace App\Http\Controllers;

class SearchController extends Controller
{
    /**
     * Show search form.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function searchForm()
    {
        return view('search.index');
    }

    /**
     * Show search results.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function searchResult()
    {
        return view('search.result');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/paieska', 'SearchController@searchForm')->name('search.index');
Route::get('/paieska2', 'SearchController@searchResult')->name('search.result');
Route::get('/receptai-pagal-tipa', 'RecipeController@index')->name('recipe.index');
Route::get('/straipsniai', 'ArticleController@index')->name('article.index');
Route::get('/videoreceptai-pagal-tipa', 'VideorecipeController@index')->name('videorecipe.index');
Route::get('/forumas', 'ForumController@index')->name('forum.index');

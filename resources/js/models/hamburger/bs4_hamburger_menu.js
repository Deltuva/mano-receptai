const scroll_height = 100,
  hamburger = document.querySelector(".hamburger"),
  bootstrap_navbar = document.querySelector(".navbar"),
  bootstrap_navbar_collapse = document.querySelector(".navbar-collapse");

// add active class is-active to hamburger
hamburger.onclick = () => hamburger.classList.toggle("is-active");

const scrollIt = () => {
  if (window.scrollY > scroll_height) {
    bootstrap_navbar.classList.add("sticky-fixed");
  } else {
    bootstrap_navbar.classList.remove("sticky-fixed");
    hamburger.classList.remove("is-active");
  }
};

window.addEventListener("scroll", scrollIt);

// init
scrollIt();

@extends('layouts.app')
@section('content')

<main class="bg-fullscreen">
    <div class="bg-fullscreen-overlay">
        <div class="d-none d-md-block d-lg-block bg-fullscreen-overlay-title">
            <h1>TEGUL BŪNA SKANU.</h1>
            <h2>ManoReceptai</h2>
        </div>
    </div>
</main>

<section class="sect-article">
    <div class="container">
        <div class="row">
            <div class="head mb-5 col-md-12 hidden-xs">
                <h2>{{ __("Straipsniai") }}</h2>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <form class="filter-form" data-action="" method="POST">
                            @csrf

                            <div class="form-group">
                                <label for="colFormLabelLg"
                                    class="col-sm-12 col-form-label col-form-label-lg text-left justify-content-left pl-0">
                                    {{ __("Rūšiavimas pagal") }}:
                                </label>
                                <select class="form-control rounded-0 custom-select">
                                    <option value="" selected>--</option>
                                    <option value="">{{ __('Pagal naujumą') }}</option>
                                    <option value="">{{ __('Pagal įvertinimą') }}</option>
                                    <option value="">{{ __('Pagal populiarumą') }}</option>
                                    <option value="">{{ __('Pagal pavadinimą') }}</option>
                                    <option value="">{{ __('Pagal komentarų sk.') }}</option>
                                </select>
                            </div>

                        </form>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">

                    <div class="col-sm-12">

                        <div class="article-block">

                            <div class="row">

                                <div class="col-lg-9 col-8">
                                    <div class="article-item">
                                        <a href="#">
                                            <h1 class="article-item__title">Žalingi indai: ką palikti, o ką - išmesti?
                                            </h1>
                                        </a>

                                        <p class="article-item__description">
                                            Anksčiau cinkuotų ir emaliuotų puodų, vietinės gamybos keptuvių kokybė visus
                                            tenkino. Apie kenksmingas medžiagas ir galimus apsinuodijimus niekas
                                            negalvojo. Dabar viskas pakito ir ne visi įsigyti indai yra saugūs.
                                            Panagrinėkime kokį kenksmingą poveikį sveikatai gal...
                                        </p>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-4">
                                    <div class="article-image">
                                        <img src="https://source.unsplash.com/400x300/?cooking" alt=""
                                            class="img-fluid">
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!-- /.article-block -->

                        <div class="article-block">

                            <div class="row">

                                <div class="col-lg-9 col-8">
                                    <div class="article-item">
                                        <a href="#">
                                            <h1 class="article-item__title">Žalingi indai: ką palikti, o ką - išmesti? (1)
                                                <span class="article-item__title-comments"><i
                                                        class="fas fa-comments"></i></span>
                                            </h1>
                                        </a>

                                        <p class="article-item__description">
                                            Anksčiau cinkuotų ir emaliuotų puodų, vietinės gamybos keptuvių kokybė visus
                                            tenkino. Apie kenksmingas medžiagas ir galimus apsinuodijimus niekas
                                            negalvojo. Dabar viskas pakito ir ne visi įsigyti indai yra saugūs.
                                            Panagrinėkime kokį kenksmingą poveikį sveikatai gal...
                                        </p>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-4">
                                    <div class="article-image">
                                        <img src="https://source.unsplash.com/400x300/?cooking" alt=""
                                            class="img-fluid">
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!-- /.article-block -->

                        <div class="article-block">

                            <div class="row">

                                <div class="col-lg-9 col-8">
                                    <div class="article-item">
                                        <a href="#">
                                            <h1 class="article-item__title">Žalingi indai: ką palikti, o ką - išmesti?</h1>
                                        </a>

                                        <p class="article-item__description">
                                            Anksčiau cinkuotų ir emaliuotų puodų, vietinės gamybos keptuvių kokybė visus
                                            tenkino. Apie kenksmingas medžiagas ir galimus apsinuodijimus niekas
                                            negalvojo. Dabar viskas pakito ir ne visi įsigyti indai yra saugūs.
                                            Panagrinėkime kokį kenksmingą poveikį sveikatai gal...
                                        </p>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-4">
                                    <div class="article-image">
                                        <img src="https://source.unsplash.com/400x300/?cooking" alt=""
                                            class="img-fluid">
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!-- /.article-block -->

                        <div class="article-block">

                            <div class="row">

                                <div class="col-lg-9 col-8">
                                    <div class="article-item">
                                        <a href="#">
                                            <h1 class="article-item__title">Žalingi indai: ką palikti, o ką - išmesti?</h1>
                                        </a>

                                        <p class="article-item__description">
                                            Anksčiau cinkuotų ir emaliuotų puodų, vietinės gamybos keptuvių kokybė visus
                                            tenkino. Apie kenksmingas medžiagas ir galimus apsinuodijimus niekas
                                            negalvojo. Dabar viskas pakito ir ne visi įsigyti indai yra saugūs.
                                            Panagrinėkime kokį kenksmingą poveikį sveikatai gal...
                                        </p>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-4">
                                    <div class="article-image">
                                        <img src="https://source.unsplash.com/400x300/?cooking" alt=""
                                            class="img-fluid">
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!-- /.article-block -->

                        <div class="article-block">

                            <div class="row">

                                <div class="col-lg-9 col-8">
                                    <div class="article-item">
                                        <a href="#">
                                            <h1 class="article-item__title">Žalingi indai: ką palikti, o ką - išmesti?</h1>
                                        </a>

                                        <p class="article-item__description">
                                            Anksčiau cinkuotų ir emaliuotų puodų, vietinės gamybos keptuvių kokybė visus
                                            tenkino. Apie kenksmingas medžiagas ir galimus apsinuodijimus niekas
                                            negalvojo. Dabar viskas pakito ir ne visi įsigyti indai yra saugūs.
                                            Panagrinėkime kokį kenksmingą poveikį sveikatai gal...
                                        </p>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-4">
                                    <div class="article-image">
                                        <img src="https://source.unsplash.com/400x300/?cooking" alt=""
                                            class="img-fluid">
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!-- /.article-block -->

                        <div class="article-block">

                            <div class="row">

                                <div class="col-lg-9 col-8">
                                    <div class="article-item">
                                        <a href="#">
                                            <h1 class="article-item__title">Žalingi indai: ką palikti, o ką - išmesti?</h1>
                                        </a>

                                        <p class="article-item__description">
                                            Anksčiau cinkuotų ir emaliuotų puodų, vietinės gamybos keptuvių kokybė visus
                                            tenkino. Apie kenksmingas medžiagas ir galimus apsinuodijimus niekas
                                            negalvojo. Dabar viskas pakito ir ne visi įsigyti indai yra saugūs.
                                            Panagrinėkime kokį kenksmingą poveikį sveikatai gal...
                                        </p>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-4">
                                    <div class="article-image">
                                        <img src="https://source.unsplash.com/400x300/?cooking" alt=""
                                            class="img-fluid">
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!-- /.article-block -->

                        <div class="article-block">

                            <div class="row">

                                <div class="col-lg-9 col-8">
                                    <div class="article-item">
                                        <a href="#">
                                            <h1 class="article-item__title">Žalingi indai: ką palikti, o ką - išmesti?</h1>
                                        </a>

                                        <p class="article-item__description">
                                            Anksčiau cinkuotų ir emaliuotų puodų, vietinės gamybos keptuvių kokybė visus
                                            tenkino. Apie kenksmingas medžiagas ir galimus apsinuodijimus niekas
                                            negalvojo. Dabar viskas pakito ir ne visi įsigyti indai yra saugūs.
                                            Panagrinėkime kokį kenksmingą poveikį sveikatai gal...
                                        </p>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-4">
                                    <div class="article-image">
                                        <img src="https://source.unsplash.com/400x300/?cooking" alt=""
                                            class="img-fluid">
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!-- /.article-block -->

                        <div class="article-block">

                            <div class="row">

                                <div class="col-lg-9 col-8">
                                    <div class="article-item">
                                        <a href="#">
                                            <h1 class="article-item__title">Žalingi indai: ką palikti, o ką - išmesti?</h1>
                                        </a>

                                        <p class="article-item__description">
                                            Anksčiau cinkuotų ir emaliuotų puodų, vietinės gamybos keptuvių kokybė visus
                                            tenkino. Apie kenksmingas medžiagas ir galimus apsinuodijimus niekas
                                            negalvojo. Dabar viskas pakito ir ne visi įsigyti indai yra saugūs.
                                            Panagrinėkime kokį kenksmingą poveikį sveikatai gal...
                                        </p>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-4">
                                    <div class="article-image">
                                        <img src="https://source.unsplash.com/400x300/?cooking" alt=""
                                            class="img-fluid">
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!-- /.article-block -->

                    </div>

                </div>

                <div class="d-flex justify-content-center article-more my-5">
                    <a href="#" class="btn btn-primary rounded-0">{{ __('Daugiau straipsnių') }}</a>
                </div>
            </div>

        </div>
    </div>
</section>

@endsection
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
</head>

<body>
    <div id="appc">
        <header class="main-header">
            <div class="header-container">

                <div class="topline-container">
                    <nav class="navbar navbar-expand-lg fixed-top">
                        <div class="container">
                            <a class="navbar-brand" href="{{ url('/') }}">
                                <img src="{{ asset('img/logo.png') }}" alt="Logo">
                            </a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                                <div class="hamburger hamburger--collapse js-hamburger">
                                    <div class="hamburger-box">
                                        <div class="hamburger-inner"></div>
                                    </div>
                                </div>
                                <div>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <!-- Left Side Of Navbar -->
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item"><a class="nav-link" href="{{ route('search.index') }}">Paieška</a></li>
                                    <li class="nav-item"><a class="nav-link" href="{{ route('recipe.index') }}">Receptai</a></li>
                                    <li class="nav-item"><a class="nav-link" href="{{ route('article.index') }}">Straipsniai</a></li>
                                    <li class="nav-item"><a class="nav-link" href="{{ route('videorecipe.index') }}">Videoreceptai</a></li>
                                    <li class="nav-item"><a class="nav-link" href="{{ route('forum.index') }}">Forumas</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#">Žodynas</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#">Žaidimai</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#">Nariai</a></li>
                                </ul>

                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <img src="{{ asset('images/avatar.jpg') }}"
                                                class="avatar img-fluid rounded-0 mr-2" style="width: 60px;"> Mindaugas
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-left rounded-0"
                                            aria-labelledby="navbarDropdownMenuLink">
                                            <a class="dropdown-item" href="#">Atsiųsti receptą</a>
                                            <a class="dropdown-item" href="#">Mano meniu</a>
                                        </div>
                                    </li>
                                </ul>

                                <!-- Right Side Of Navbar -->
                                {{-- <ul class="navbar-nav ml-auto">
                                    <!-- Authentication Links -->
                                    @guest
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                                @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                                @endif
                                @else
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                                document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                            style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                                @endguest
                                </ul> --}}
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </header>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
</body>

</html>
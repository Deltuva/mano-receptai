@extends('layouts.app')
@section('content')

<main class="bg-fullscreen">
    <div class="bg-fullscreen-overlay">
        <div class="d-none d-md-block d-lg-block bg-fullscreen-overlay-title">
            <h1>TEGUL BŪNA SKANU.</h1>
            <h2>ManoReceptai</h2>
        </div>
    </div>
</main>

<section class="sect-home">
    <div class="container">
        <div class="row">
            <div class="head mb-5 col-md-12 hidden-xs">
                <h2>{{ __("Naujausi receptai") }}</h2>
            </div>

            <div class="container">
                <div class="row">

                    <div class="col-sm-12">

                        <div class="recent-block">

                            <div class="row">

                                <div class="col-lg-9 col-8">
                                    <div class="recent-item">
                                        <a href="#">
                                            <h1 class="recent-item__title">
                                                <i class="fas fa-receipt mr-2"></i> Žalias kokteilis
                                            </h1>
                                        </a>

                                        <p class="recent-item__description">
                                            Anksčiau cinkuotų ir emaliuotų puodų, vietinės gamybos keptuvių kokybė visus
                                            tenkino. Apie kenksmingas medžiagas ir galimus apsinuodijimus niekas
                                            negalvojo. Dabar viskas pakito ir ne visi įsigyti indai yra saugūs.
                                            Panagrinėkime kokį kenksmingą poveikį sveikatai gal...
                                        </p>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-4">
                                    <div class="recent-image">
                                        <img src="https://source.unsplash.com/400x300/?cooking" alt=""
                                            class="img-fluid">
                                    </div>
                                </div>

                            </div>
                            <!-- /.row -->

                            <div class="d-none d-sm-block d-lg-block recent-bottom">

                                <div class="gradients">
                                    <i class="fas fa-american-sign-language-interpreting"></i> Ingredientai:

                                    <ul class="gradients__list">
                                        <li class="gradients__list-item active">1 bananas</li>
                                        <li class="gradients__list-item">3 kiviai</li>
                                        <li class="gradients__list-item">datules</li>
                                        <li class="gradients__list-item">vandens</li>
                                        <li class="gradients__list-item">1 obuolis raudonas</li>
                                    </ul>
                                </div>

                            </div>
                            <!-- /.recent-bottom -->

                        </div>
                        <!-- /.recent-block -->

                        <div class="recent-block">

                            <div class="row">

                                <div class="col-lg-9 col-8">
                                    <div class="recent-item">
                                        <a href="#">
                                            <h1 class="recent-item__title">
                                                <i class="fas fa-receipt mr-2"></i> Žalias kokteilis
                                            </h1>
                                        </a>

                                        <p class="recent-item__description">
                                            Anksčiau cinkuotų ir emaliuotų puodų, vietinės gamybos keptuvių kokybė visus
                                            tenkino. Apie kenksmingas medžiagas ir galimus apsinuodijimus niekas
                                            negalvojo. Dabar viskas pakito ir ne visi įsigyti indai yra saugūs.
                                            Panagrinėkime kokį kenksmingą poveikį sveikatai gal...
                                        </p>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-4">
                                    <div class="recent-image">
                                        <img src="https://source.unsplash.com/400x300/?cooking" alt=""
                                            class="img-fluid">
                                    </div>
                                </div>

                            </div>
                            <!-- /.row -->

                            <div class="d-none d-sm-block d-lg-block recent-bottom">

                                <div class="gradients">
                                    <i class="fas fa-american-sign-language-interpreting"></i> Ingredientai:

                                    <ul class="gradients__list">
                                        <li class="gradients__list-item active">1 bananas</li>
                                        <li class="gradients__list-item">3 kiviai</li>
                                        <li class="gradients__list-item">datules</li>
                                        <li class="gradients__list-item">vandens</li>
                                        <li class="gradients__list-item">1 obuolis raudonas</li>
                                    </ul>
                                </div>

                            </div>
                            <!-- /.recent-bottom -->

                        </div>
                        <!-- /.recent-block -->

                        <div class="recent-block">

                            <div class="row">

                                <div class="col-lg-9 col-8">
                                    <div class="recent-item">
                                        <a href="#">
                                            <h1 class="recent-item__title">
                                                <i class="fas fa-receipt mr-2"></i> Žalias kokteilis
                                            </h1>
                                        </a>

                                        <p class="recent-item__description">
                                            Anksčiau cinkuotų ir emaliuotų puodų, vietinės gamybos keptuvių kokybė visus
                                            tenkino. Apie kenksmingas medžiagas ir galimus apsinuodijimus niekas
                                            negalvojo. Dabar viskas pakito ir ne visi įsigyti indai yra saugūs.
                                            Panagrinėkime kokį kenksmingą poveikį sveikatai gal...
                                        </p>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-4">
                                    <div class="recent-image">
                                        <img src="https://source.unsplash.com/400x300/?cooking" alt=""
                                            class="img-fluid">
                                    </div>
                                </div>

                            </div>
                            <!-- /.row -->

                            <div class="d-none d-sm-block d-lg-block recent-bottom">

                                <div class="gradients">
                                    <i class="fas fa-american-sign-language-interpreting"></i> Ingredientai:

                                    <ul class="gradients__list">
                                        <li class="gradients__list-item active">1 bananas</li>
                                        <li class="gradients__list-item">3 kiviai</li>
                                        <li class="gradients__list-item">datules</li>
                                        <li class="gradients__list-item">vandens</li>
                                        <li class="gradients__list-item">1 obuolis raudonas</li>
                                    </ul>
                                </div>

                            </div>
                            <!-- /.recent-bottom -->

                        </div>
                        <!-- /.recent-block -->

                        <div class="recent-block">

                            <div class="row">

                                <div class="col-lg-9 col-8">
                                    <div class="recent-item">
                                        <a href="#">
                                            <h1 class="recent-item__title">
                                                <i class="fas fa-receipt mr-2"></i> Žalias kokteilis
                                            </h1>
                                        </a>

                                        <p class="recent-item__description">
                                            Anksčiau cinkuotų ir emaliuotų puodų, vietinės gamybos keptuvių kokybė visus
                                            tenkino. Apie kenksmingas medžiagas ir galimus apsinuodijimus niekas
                                            negalvojo. Dabar viskas pakito ir ne visi įsigyti indai yra saugūs.
                                            Panagrinėkime kokį kenksmingą poveikį sveikatai gal...
                                        </p>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-4">
                                    <div class="recent-image">
                                        <img src="https://source.unsplash.com/400x300/?cooking" alt=""
                                            class="img-fluid">
                                    </div>
                                </div>

                            </div>
                            <!-- /.row -->

                            <div class="d-none d-sm-block d-lg-block recent-bottom">

                                <div class="gradients">
                                    <i class="fas fa-american-sign-language-interpreting"></i> Ingredientai:

                                    <ul class="gradients__list">
                                        <li class="gradients__list-item active">1 bananas</li>
                                        <li class="gradients__list-item">3 kiviai</li>
                                        <li class="gradients__list-item">datules</li>
                                        <li class="gradients__list-item">vandens</li>
                                        <li class="gradients__list-item">1 obuolis raudonas</li>
                                    </ul>
                                </div>

                            </div>
                            <!-- /.recent-bottom -->

                        </div>
                        <!-- /.recent-block -->

                        <div class="recent-block">

                            <div class="row">

                                <div class="col-lg-9 col-8">
                                    <div class="recent-item">
                                        <a href="#">
                                            <h1 class="recent-item__title">
                                                <i class="fas fa-receipt mr-2"></i> Žalias kokteilis
                                            </h1>
                                        </a>

                                        <p class="recent-item__description">
                                            Anksčiau cinkuotų ir emaliuotų puodų, vietinės gamybos keptuvių kokybė visus
                                            tenkino. Apie kenksmingas medžiagas ir galimus apsinuodijimus niekas
                                            negalvojo. Dabar viskas pakito ir ne visi įsigyti indai yra saugūs.
                                            Panagrinėkime kokį kenksmingą poveikį sveikatai gal...
                                        </p>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-4">
                                    <div class="recent-image">
                                        <img src="https://source.unsplash.com/400x300/?cooking" alt=""
                                            class="img-fluid">
                                    </div>
                                </div>

                            </div>
                            <!-- /.row -->

                            <div class="d-none d-sm-block d-lg-block recent-bottom">

                                <div class="gradients">
                                    <i class="fas fa-american-sign-language-interpreting"></i> Ingredientai:

                                    <ul class="gradients__list">
                                        <li class="gradients__list-item active">1 bananas</li>
                                        <li class="gradients__list-item">3 kiviai</li>
                                        <li class="gradients__list-item">datules</li>
                                        <li class="gradients__list-item">vandens</li>
                                        <li class="gradients__list-item">1 obuolis raudonas</li>
                                    </ul>
                                </div>

                            </div>
                            <!-- /.recent-bottom -->

                        </div>
                        <!-- /.recent-block -->

                        <div class="recent-block">

                            <div class="row">

                                <div class="col-lg-9 col-8">
                                    <div class="recent-item">
                                        <a href="#">
                                            <h1 class="recent-item__title">
                                                <i class="fas fa-receipt mr-2"></i> Žalias kokteilis
                                            </h1>
                                        </a>

                                        <p class="recent-item__description">
                                            Anksčiau cinkuotų ir emaliuotų puodų, vietinės gamybos keptuvių kokybė visus
                                            tenkino. Apie kenksmingas medžiagas ir galimus apsinuodijimus niekas
                                            negalvojo. Dabar viskas pakito ir ne visi įsigyti indai yra saugūs.
                                            Panagrinėkime kokį kenksmingą poveikį sveikatai gal...
                                        </p>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-4">
                                    <div class="recent-image">
                                        <img src="https://source.unsplash.com/400x300/?cooking" alt=""
                                            class="img-fluid">
                                    </div>
                                </div>

                            </div>
                            <!-- /.row -->

                            <div class="d-none d-sm-block d-lg-block recent-bottom">

                                <div class="gradients">
                                    <i class="fas fa-american-sign-language-interpreting"></i> Ingredientai:

                                    <ul class="gradients__list">
                                        <li class="gradients__list-item active">1 bananas</li>
                                        <li class="gradients__list-item">3 kiviai</li>
                                        <li class="gradients__list-item">datules</li>
                                        <li class="gradients__list-item">vandens</li>
                                        <li class="gradients__list-item">1 obuolis raudonas</li>
                                    </ul>
                                </div>

                            </div>
                            <!-- /.recent-bottom -->

                        </div>
                        <!-- /.recent-block -->

                        <div class="recent-block">

                            <div class="row">

                                <div class="col-lg-9 col-8">
                                    <div class="recent-item">
                                        <a href="#">
                                            <h1 class="recent-item__title">
                                                <i class="fas fa-receipt mr-2"></i> Žalias kokteilis
                                            </h1>
                                        </a>

                                        <p class="recent-item__description">
                                            Anksčiau cinkuotų ir emaliuotų puodų, vietinės gamybos keptuvių kokybė visus
                                            tenkino. Apie kenksmingas medžiagas ir galimus apsinuodijimus niekas
                                            negalvojo. Dabar viskas pakito ir ne visi įsigyti indai yra saugūs.
                                            Panagrinėkime kokį kenksmingą poveikį sveikatai gal...
                                        </p>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-4">
                                    <div class="recent-image">
                                        <img src="https://source.unsplash.com/400x300/?cooking" alt=""
                                            class="img-fluid">
                                    </div>
                                </div>

                            </div>
                            <!-- /.row -->

                            <div class="d-none d-sm-block d-lg-block recent-bottom">

                                <div class="gradients">
                                    <i class="fas fa-american-sign-language-interpreting"></i> Ingredientai:

                                    <ul class="gradients__list">
                                        <li class="gradients__list-item active">1 bananas</li>
                                        <li class="gradients__list-item">3 kiviai</li>
                                        <li class="gradients__list-item">datules</li>
                                        <li class="gradients__list-item">vandens</li>
                                        <li class="gradients__list-item">1 obuolis raudonas</li>
                                    </ul>
                                </div>

                            </div>
                            <!-- /.recent-bottom -->

                        </div>
                        <!-- /.recent-block -->

                        <div class="recent-block">

                            <div class="row">

                                <div class="col-lg-9 col-8">
                                    <div class="recent-item">
                                        <a href="#">
                                            <h1 class="recent-item__title">
                                                <i class="fas fa-receipt mr-2"></i> Žalias kokteilis
                                            </h1>
                                        </a>

                                        <p class="recent-item__description">
                                            Anksčiau cinkuotų ir emaliuotų puodų, vietinės gamybos keptuvių kokybė visus
                                            tenkino. Apie kenksmingas medžiagas ir galimus apsinuodijimus niekas
                                            negalvojo. Dabar viskas pakito ir ne visi įsigyti indai yra saugūs.
                                            Panagrinėkime kokį kenksmingą poveikį sveikatai gal...
                                        </p>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-4">
                                    <div class="recent-image">
                                        <img src="https://source.unsplash.com/400x300/?cooking" alt=""
                                            class="img-fluid">
                                    </div>
                                </div>

                            </div>
                            <!-- /.row -->

                            <div class="d-none d-sm-block d-lg-block recent-bottom">

                                <div class="gradients">
                                    <i class="fas fa-american-sign-language-interpreting"></i> Ingredientai:

                                    <ul class="gradients__list">
                                        <li class="gradients__list-item active">1 bananas</li>
                                        <li class="gradients__list-item">3 kiviai</li>
                                        <li class="gradients__list-item">datules</li>
                                        <li class="gradients__list-item">vandens</li>
                                        <li class="gradients__list-item">1 obuolis raudonas</li>
                                    </ul>
                                </div>

                            </div>
                            <!-- /.recent-bottom -->

                        </div>
                        <!-- /.recent-block -->

                    </div>

                </div>

                <div class="d-flex justify-content-center recent-more my-5">
                    <a href="#" class="btn btn-primary rounded-0">{{ __('Daugiau receptų') }}</a>
                </div>
            </div>

        </div>
    </div>
</section>

@endsection
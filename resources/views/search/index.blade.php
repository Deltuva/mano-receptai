@extends('layouts.app')
@section('content')

<main class="bg-fullscreen">
    <div class="bg-fullscreen-overlay">
        <div class="d-none d-md-block d-lg-block bg-fullscreen-overlay-title">
            <h1>TEGUL BŪNA SKANU.</h1>
            <h2>ManoReceptai</h2>
        </div>
    </div>
</main>

<section class="sect-search">
    <div class="container">
        <div class="row">
            <div class="head mb-4 col-md-12 hidden-xs">
                <h2>{{ __("Receptų paieška") }}</h2>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="search-form">
                            <form data-action="" method="POST">
                                @csrf

                                <div class="form-group mb-5">
                                    <label
                                        for="colFormLabelLg"
                                        class="col-sm-12 col-form-label col-form-label-lg text-left justify-content-left pl-0"
                                    >
                                        {{ __("Ieškoti tarp") }}:
                                    </label>
                                    <select
                                        class="form-control rounded-0 custom-select"
                                    >
                                        <option value="" selected>--</option>
                                    </select>

                                    <label
                                        for="colFormLabelLg"
                                        class="col-sm-12 col-form-label col-form-label-lg text-left justify-content-left pl-0"
                                    >
                                        {{ __("Ko ieškote (užklausa)") }}:
                                    </label>
                                    <input
                                        type="search"
                                        name="search"
                                        id="search"
                                        value=""
                                        placeholder="{{
                                            __('Įveskite ko ieškote ...')
                                        }}"
                                        class="form-control form-control-lg rounded-0"
                                        aria-label="{{ __('Paieška') }}"
                                    />
                                </div>

                                <div class="form-group row pull-right">
                                    <div class="col-lg-2 col-md-2 col-xs-12">
                                        <button
                                            type="submit"
                                            class="btn btn-primary rounded-0"
                                        >
                                            {{ __("Ieškoti") }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

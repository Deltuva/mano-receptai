@extends('layouts.app')
@section('content')

<main class="bg-fullscreen">
    <div class="bg-fullscreen-overlay">
        <div class="d-none d-md-block d-lg-block bg-fullscreen-overlay-title">
            <h1>TEGUL BŪNA SKANU.</h1>
            <h2>ManoReceptai</h2>
        </div>
    </div>
</main>

<section class="sect-recipe">
    <div class="container">
        <div class="row">
            <div class="head mb-5 col-md-12 hidden-xs">
                <h2>{{ __("Visi Receptai") }}</h2>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <form class="filter-form" data-action="" method="POST">
                            @csrf

                            <div class="form-group">
                                <label for="colFormLabelLg"
                                    class="col-sm-12 col-form-label col-form-label-lg text-left justify-content-left pl-0">
                                    {{ __("Rūšiavimas pagal") }}:
                                </label>
                                <select class="form-control rounded-0 custom-select">
                                    <option value="" selected>--</option>
                                    <option value="">{{ __('Pagal tipą') }}</option>
                                    <option value="">{{ __('Pagal sudėtį') }}</option>
                                    <option value="">{{ __('Pagal ruošimo būdą') }}</option>
                                    <option value="">{{ __('Pagal šalį') }}</option>
                                    <option value="">{{ __('Pagal pobūdį') }}</option>
                                </select>
                            </div>

                        </form>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">

                    <div class="col-lg-4 col-sm-6 col-12">

                        <div class="recipe-item">
                            <a href="page.html" class="recipe-item-image">
                                <img src="https://source.unsplash.com/400x300/?cooking" alt="" class="img-fluid">
                                <div class="recipe-item-title">
                                    {{ __('3 šviežių sriubų receptai') }}
                                </div>
                            </a>
                        </div>

                    </div>
                    <!-- /.recipe-item -->

                    <div class="col-lg-4 col-sm-6 col-12">

                        <div class="recipe-item">
                            <a href="page.html" class="recipe-item-image">
                                <img src="https://source.unsplash.com/400x300/?cooking" alt="" class="img-fluid">
                                <div class="d-flex align-items-center recipe-item-title">
                                    {{ __('Aitrūs vištų sparneliai') }}
                                </div>
                            </a>
                        </div>

                    </div>
                    <!-- /.recipe-item -->

                    <div class="col-lg-4 col-sm-6 col-12">

                        <div class="recipe-item">
                            <a href="page.html" class="recipe-item-image">
                                <img src="https://source.unsplash.com/400x300/?cooking" alt="" class="img-fluid">
                                <div class="d-flex align-items-center recipe-item-title">
                                    {{ __('Avienos šonkauliai') }}
                                </div>
                            </a>
                        </div>

                    </div>
                    <!-- /.recipe-item -->

                    <div class="col-lg-4 col-sm-6 col-12">

                        <div class="recipe-item">
                            <a href="page.html" class="recipe-item-image">
                                <img src="https://source.unsplash.com/400x300/?cooking" alt="" class="img-fluid">
                                <div class="d-flex align-items-center recipe-item-title">
                                    {{ __('Avienos šonkauliai') }}
                                </div>
                            </a>
                        </div>

                    </div>
                    <!-- /.recipe-item -->

                    <div class="col-lg-4 col-sm-6 col-12">

                        <div class="recipe-item">
                            <a href="page.html" class="recipe-item-image">
                                <img src="https://source.unsplash.com/400x300/?cooking" alt="" class="img-fluid">
                                <div class="recipe-item-title">
                                    {{ __('3 šviežių sriubų receptai') }}
                                </div>
                            </a>
                        </div>

                    </div>
                    <!-- /.recipe-item -->

                    <div class="col-lg-4 col-sm-6 col-12">

                        <div class="recipe-item">
                            <a href="page.html" class="recipe-item-image">
                                <img src="https://source.unsplash.com/400x300/?cooking" alt="" class="img-fluid">
                                <div class="d-flex align-items-center recipe-item-title">
                                    {{ __('Aitrūs vištų sparneliai') }}
                                </div>
                            </a>
                        </div>

                    </div>
                    <!-- /.recipe-item -->

                    <div class="col-lg-4 col-sm-6 col-12">

                        <div class="recipe-item">
                            <a href="page.html" class="recipe-item-image">
                                <img src="https://source.unsplash.com/400x300/?cooking" alt="" class="img-fluid">
                                <div class="d-flex align-items-center recipe-item-title">
                                    {{ __('Avienos šonkauliai') }}
                                </div>
                            </a>
                        </div>

                    </div>
                    <!-- /.recipe-item -->

                    <div class="col-lg-4 col-sm-6 col-12">

                        <div class="recipe-item">
                            <a href="page.html" class="recipe-item-image">
                                <img src="https://source.unsplash.com/400x300/?cooking" alt="" class="img-fluid">
                                <div class="d-flex align-items-center recipe-item-title">
                                    {{ __('Avienos šonkauliai') }}
                                </div>
                            </a>
                        </div>

                    </div>
                    <!-- /.recipe-item -->

                    <div class="col-lg-4 col-sm-6 col-12">

                        <div class="recipe-item">
                            <a href="page.html" class="recipe-item-image">
                                <img src="https://source.unsplash.com/400x300/?cooking" alt="" class="img-fluid">
                                <div class="recipe-item-title">
                                    {{ __('3 šviežių sriubų receptai') }}
                                </div>
                            </a>
                        </div>

                    </div>
                    <!-- /.recipe-item -->

                    <div class="col-lg-4 col-sm-6 col-12">

                        <div class="recipe-item">
                            <a href="page.html" class="recipe-item-image">
                                <img src="https://source.unsplash.com/400x300/?cooking" alt="" class="img-fluid">
                                <div class="d-flex align-items-center recipe-item-title">
                                    {{ __('Aitrūs vištų sparneliai') }}
                                </div>
                            </a>
                        </div>

                    </div>
                    <!-- /.recipe-item -->

                    <div class="col-lg-4 col-sm-6 col-12">

                        <div class="recipe-item">
                            <a href="page.html" class="recipe-item-image">
                                <img src="https://source.unsplash.com/400x300/?cooking" alt="" class="img-fluid">
                                <div class="d-flex align-items-center recipe-item-title">
                                    {{ __('Avienos šonkauliai') }}
                                </div>
                            </a>
                        </div>

                    </div>
                    <!-- /.recipe-item -->

                    <div class="col-lg-4 col-sm-6 col-12">

                        <div class="recipe-item">
                            <a href="page.html" class="recipe-item-image">
                                <img src="https://source.unsplash.com/400x300/?cooking" alt="" class="img-fluid">
                                <div class="d-flex align-items-center recipe-item-title">
                                    {{ __('Avienos šonkauliai') }}
                                </div>
                            </a>
                        </div>

                    </div>
                    <!-- /.recipe-item -->

                </div>

                <div class="d-flex justify-content-center recipe-more my-5">
                    <a href="#" class="btn btn-primary rounded-0">{{ __('Daugiau receptų') }}</a>
                </div>
            </div>

        </div>
    </div>
</section>

@endsection